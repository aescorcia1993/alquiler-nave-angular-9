import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

@Injectable({providedIn: 'root'})
export class PanelGuard implements CanActivate {
  constructor() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let auth = JSON.parse(localStorage.getItem("userSession"))
    if (auth?.role === "admin" || auth?.role === "piloto" ){
      return true
    }else{
      alert("Su perfil no esta autorizado para acceder.")
      return false
    }
  }
}
