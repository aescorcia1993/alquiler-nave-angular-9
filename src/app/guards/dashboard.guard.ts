import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import userInt from 'src/models/userModel';

@Injectable({providedIn: 'root'})
export class DashboardGuard implements CanActivate {
  constructor( private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
     let auth:userInt  =  JSON.parse(localStorage.getItem('userSession'))
    if(auth?.user.length > 0){
       return true
    }else{
      alert("Usted no ha iniciado sesión, no puede acceder.")
       this.router.navigate(["login"]);
       return false
    }
  }
}
