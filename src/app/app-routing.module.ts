import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { DashboardGuard } from './guards/dashboard.guard';

const routes : Routes = [

  //Rutas alquiler de aeronave
  {path:'login', component: LoginComponent},
  {path:'', component: LoginComponent},
  {
    path: 'dashboard',
    loadChildren: ()=> import ('./componentes/modulo-lista/modulo-lista.module').then(m => m.ModuloListaModule),
    canActivate: [DashboardGuard]
  }
  ]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
