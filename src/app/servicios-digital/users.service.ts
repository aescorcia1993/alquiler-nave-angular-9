import { Injectable } from '@angular/core';
import myUsers  from 'src/assets/mocks/users.json';
import permissions  from 'src/assets/mocks/permission.json';
import myTrips from 'src/assets/mocks/trips.json'
import userInt from 'src/models/userModel';
import { Observable, Subject } from 'rxjs';
import viaje from 'src/models/viajeModel';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users = new Subject<userInt[]>();
  $users = this.users.asObservable();

  permission = new Subject<{}[]>();
  $permission = this.permission.asObservable();

  trips = new Subject<viaje[]>();
  $trip = this.trips.asObservable();

  history = new Subject<{}[]>();
  $history = this.history.asObservable();

  constructor() {}

getUsers(){
  if (!this.isOldSession()){
    localStorage.setItem('myUsers', JSON.stringify(myUsers) )
    this.users.next(myUsers)
  }else{
    let myUsers : any = JSON.parse(localStorage.getItem('myUsers'));
    this.users.next(myUsers)
  }

}
 getPermissions(){
    this.permission.next(permissions);
}
getViajes(){
  if (!this.isOldSession() ){
    localStorage.setItem('myTrips', JSON.stringify(myTrips) )
    this.trips.next(myTrips)
  }else{
    let myTrips2:{}[] = JSON.parse(localStorage.getItem('myTrips'));
    this.trips.next(myTrips2)
  }
}

bloquearBorradoSession(){
    localStorage.setItem("oldSession","yes")
}
isOldSession():boolean{
  return localStorage.getItem("oldSession") === 'yes'?true:false
}
// viajes methods
editarViaje(oldDataTrip:viaje,newDataTrip:viaje,myTrips:viaje[]){
  let newArray:viaje[] =   myTrips.map((trip)=>{
    return  trip.id === oldDataTrip.id? newDataTrip : trip ;
  })

  localStorage.setItem('myTrips', JSON.stringify(newArray))
  this.trips.next(newArray)
}
//permission methods

//history methods

//user methods
 borrar(user:userInt,myUsers:userInt[]){
    let newListUsers:userInt[] =  myUsers.filter((userI)=> userI.id !== user.id)
    localStorage.setItem('myUsers', JSON.stringify(newListUsers))
    this.users.next(newListUsers)
 }
 editar(oldDataUser:userInt,newDataUser:userInt,myUsers:userInt[]){
    let newArray:userInt[] =   myUsers.map((user)=>{
      return  user.id !== oldDataUser.id? user : newDataUser
    })

    localStorage.setItem('myUsers', JSON.stringify(newArray))
    this.users.next(newArray)
 }
 crear(newDataUser:userInt,myUsers:userInt[]){
     let newUsers = [...myUsers,newDataUser]
     this.users.next(newUsers);

     localStorage.setItem('myUsers', JSON.stringify(newUsers))
 }

}
