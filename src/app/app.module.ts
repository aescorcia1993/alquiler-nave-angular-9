import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

//components
import { AppComponent } from "./app.component";

//store installation NGRX
import { StoreModule } from "@ngrx/store";

//store-Efects
import { EffectsModule } from "@ngrx/effects";

//routing Module
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from "./componentes/login/login.component";
import { DashboardGuard } from "./guards/dashboard.guard";

//primeNG
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ButtonModule } from "primeng-lts/button";
@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ButtonModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [DashboardGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
