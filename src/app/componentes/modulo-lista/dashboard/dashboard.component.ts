import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UsersService } from "src/app/servicios-digital/users.service";
import userInt from "src/models/userModel";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
    userSession:userInt;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
        this.userSession = JSON.parse(localStorage.getItem('userSession'))
  }

  salir(){
    localStorage.removeItem('userSession')
    this.router.navigate(["login"]);
  }
}
