import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from 'src/app/servicios-digital/users.service';
import viaje from 'src/models/viajeModel';

@Component({
  selector: 'app-detalle-viaje',
  templateUrl: './detalle-viaje.component.html',
  styleUrls: ['./detalle-viaje.component.css']
})
export class DetalleViajeComponent implements OnInit {
  misViajes:viaje[];
  @Input() loaded:boolean;


  constructor(private dbService: UsersService){
    dbService.$trip.subscribe((val) => {
      this.misViajes = val;
    });
  }

  ngOnInit(): void {
    this.dbService.getViajes();
  }

  reservar(viaje:viaje){
    const newTrip = Object.assign({},viaje);
    newTrip.cupo = newTrip.cupo - 1;
    this.dbService.editarViaje(viaje, newTrip, this.misViajes)
  }
}
