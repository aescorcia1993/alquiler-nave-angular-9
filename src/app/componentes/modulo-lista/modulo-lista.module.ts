import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MenuComponent } from "./menu/menu.component";
import { ModuloListaRoutingModule } from "./modulo-lista-routing.module";
import { PanelComponent } from "./panel/panel.component";
import { AlquilerComponent } from "./alquiler/alquiler.component";
import { FormsModule } from "@angular/forms";
import { DetalleViajeComponent } from "./detalle-viaje/detalle-viaje.component";
//primeNG

import { ButtonModule } from "primeng-lts/button";
import { SplitButtonModule } from "primeng-lts/splitbutton";
import {SpinnerModule} from 'primeng-lts/spinner';
@NgModule({
  declarations: [
    DashboardComponent,
    MenuComponent,
    PanelComponent,
    AlquilerComponent,
    DetalleViajeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModuloListaRoutingModule,
    ButtonModule,
    SplitButtonModule,
    SpinnerModule
  ],
  exports: [
    DashboardComponent,
    MenuComponent,
    PanelComponent,
    DetalleViajeComponent,
  ],
})
export class ModuloListaModule {}
