import { Component, Input, OnInit } from "@angular/core";
import { UsersService } from "src/app/servicios-digital/users.service";
import userInt, { roles } from "src/models/userModel";

@Component({
  selector: "app-panel",
  templateUrl: "./panel.component.html",
  styleUrls: ["./panel.component.css"],
})
export class PanelComponent implements OnInit {
  userSession: string = "piloto";
  users: userInt[] = [];
  permission: {}[] = [];
  valid = false;
  editOrCreateUser:string ="edit";
  blankUser:userInt= {id:0,
    name:"",
    lastName:"",
    role: roles.passenger,
    password: "",
    user:""}
  // @Output() pruebaEmit = new EventEmitter<string>(); //VARIABLE QUE AL EJECUTAR EMIT EL PADRE LA ESCUCHA

  userSelected: userInt = {
    id: 0,
    name: "",
    lastName: "",
    role: roles.passenger,
    password: "",
    user: "",
  };
  userEdit: userInt = {
    id: 0,
    name: "",
    lastName: "",
    role: roles.passenger,
    password: "",
    user: "",
  };

  constructor(private userService: UsersService) {
    userService.$users.subscribe((val) => {
      this.users = val;
    });
    userService.$permission.subscribe((val) => {
      this.permission = val;
    });
  }

  ngOnInit(): void {
    this.userService.getUsers();
    this.userService.getPermissions();
    this.userSession = JSON.parse(localStorage.getItem('userSession'));
  }

  borrar(user: userInt) {
    let userPermission = this.permission.find(
      (per: any) => per.role === this.userSession['role']
    );
    let auth = userPermission["canErase"].includes(user.role);

    if (auth === true) {
      this.userService.borrar(user, this.users);
    }else{
      alert("No tienes permiso para modificar este perfil")
    }

  }

  editar(oldDataUser: userInt) {
    this.userSelected = { ...oldDataUser };
    this.userEdit = { ...oldDataUser };
    this.editOrCreateUser="edit";
  }

  guardar() {
       if (this.editOrCreateUser==="edit"){
           this.guardarEditado();
       }else{
           this.crearUsuario();
       }
  }

  guardarEditado(){
    let userPermission = this.permission.find(
      (per: any) => per.role === this.userSession['role']
    );
    let auth = userPermission["canEdit"].includes(this.userEdit.role);
    if (auth === true) {
    this.userService.editar(this.userSelected, this.userEdit, this.users);
    this.valid = false;
    this.cleanModal()
    }else{
      this.cleanModal()
      alert("No tienes permiso para modificar este perfil")
    }

  }
  crearUsuario(){

    let userPermission = this.permission.find(
      (per: any) => per.role === this.userSession['role']
    );
    let auth = userPermission["canCreate"].includes(this.userEdit.role);
    if (auth === true) {
      this.userEdit.id = this.users.length + 5;
    this.userService.crear({...this.userEdit}, this.users);
    this.valid = false;
    this.cleanModal();
    }else{
      this.cleanModal();
      alert("Solo un administrador tiene permiso para modificar este perfil")
    }

  }

  validEdit() {
    if (
      this.userEdit.role === roles.passenger ||
      this.userEdit.role === roles.piloto ||
      this.userEdit.role === roles.admin
    ) {
      this.valid = false;
    } else {
      this.valid = true;
    }
  }

  cleanModal(){
    this.userEdit = {...this.blankUser}
  }
}
