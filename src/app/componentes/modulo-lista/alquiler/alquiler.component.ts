import { Component, OnInit } from "@angular/core";
import { UsersService } from "src/app/servicios-digital/users.service";
import userInt from "src/models/userModel";

@Component({
  selector: "app-alquiler",
  templateUrl: "./alquiler.component.html",
  styleUrls: ["./alquiler.component.css"],
})
export class AlquilerComponent implements OnInit {
  viajes: {}[] = [];
  userSession: userInt;
  items: {}[] = [
    {
      label: "Update",
      icon: "pi pi-refresh",
      command: () => {
        this.save();
      },
    },
    {
      label: "Delete",
      icon: "pi pi-times",
      command: () => {
        this.save();
      },
    },
    { label: "Angular.io", icon: "pi pi-info", url: "http://angular.io" },
    { separator: true },
    { label: "Setup", icon: "pi pi-cog", routerLink: ["/login"] },
  ];


  constructor(private userService: UsersService) {
    userService.$trip.subscribe((val: {}[]) => {
      console.log("viajes", val);
      this.viajes = val;
    });
  }

  ngOnInit(): void {
    this.userService.getViajes();
    this.userService.bloquearBorradoSession();
    this.userSession = JSON.parse(localStorage.getItem("userSession"));
  }




  reservar() {

  }


  save() {
    alert("algo");
  }

}
