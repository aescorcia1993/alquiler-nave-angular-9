import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardGuard } from 'src/app/guards/dashboard.guard';
import { PanelGuard } from 'src/app/guards/panel.guard';
import { AlquilerComponent } from './alquiler/alquiler.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { PanelComponent } from './panel/panel.component';
const routes : Routes = [
  {
     path:'lista',
     canActivate: [DashboardGuard],
     children: [
       {path : '' , component: DashboardComponent,children:[
               {path : 'panel', canActivate:[PanelGuard] , component: PanelComponent},
               {path : 'alquiler', canActivate: [DashboardGuard] , component: AlquilerComponent},
               {path : 'registros', canActivate: [DashboardGuard], component: PanelComponent}
       ]//Falta el guard
      }
     ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers:[DashboardGuard ],
  exports: [RouterModule]
})
export class ModuloListaRoutingModule { }
