import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UsersService } from "src/app/servicios-digital/users.service";
import userInt from "src/models/userModel";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  user: string = "";
  pass: string = "";
  myUsers: userInt[];
  constructor(private router: Router, private userServ: UsersService) {
    userServ.$users.subscribe((val) => {
      this.myUsers = val;
    });
  }

  ngOnInit(): void {
    this.userServ.getUsers();
  }

  ingresar() {
    let auth = this.myUsers.find(
      (user) => user.user === this.user && user.password === this.pass
    );
    if (auth !== undefined) {
      this.router.navigate(["dashboard/lista/alquiler"]);
      localStorage.setItem("userSession", JSON.stringify(auth));
    } else {
      alert("No estas registrado.");
    }
  }
}
