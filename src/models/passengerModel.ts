import userInt from "./userModel";
import viaje from "./viajeModel";

interface passenger {
 user: userInt;
 trips: viaje[];
}
