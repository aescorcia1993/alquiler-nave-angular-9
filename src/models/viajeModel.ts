interface viaje {
  id:number
  nave:string
  ubicacion:string
  llegada: string
  salida: string
  pasajeros: number
  cupo: number
}
export default viaje
