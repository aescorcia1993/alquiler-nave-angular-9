interface userInt {
   id:number,
   name:string,
   lastName:string,
   role: roles,
   password: string,
   user:string
}

export enum roles {
   passenger="passenger",
   piloto="piloto",
   admin="admin"
}

export default userInt
