# BOTONES CON PRIMENG AND RESPONSIVE EXAMPLE
```html
  <div class="grid">
    <div class="lg:col-4 md:col-6 sm:col-12 col-offset-2">
      <button
        pButton
        type="button"
        icon="pi pi-check"
        iconPos="left"
        label="PRUEBA RESPONSIVE1"
      ></button>
      <button
        pButton
        type="button"
        label="Prueba"
        class="ui-button-rounded"
      ></button>
    </div>
    <div class="lg:col-4 md:col-6 sm:col-12 col-offset-2">
      <p-splitButton
        [showTransitionOptions]="'0ms'"
        [hideTransitionOptions]="'0ms'"
        label="Save"
        icon="pi pi-check"
        (onClick)="save()"
        [model]="items"
      ></p-splitButton>
    </div>
  </div>
```

